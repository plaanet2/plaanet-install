function testDependancies() {
  echo -n "install dependencies if missing (npm, git)? (y/n)"
  read -r FORCE_INSTALL
  if ! command -v npm &> /dev/null
  then
    if [ ! "$FORCE_INSTALL" = "y" ]
    then
      echo "NPM is not found !"
      exit;
    fi
    if ! command -v wget &> /dev/null
    then
      apt update
      apt install wget
    fi
    echo '### install node and npm'
    wget -qO- https://deb.nodesource.com/setup_12.x | bash
    apt update
    apt install nodejs
  fi
  if ! command -v git &> /dev/null
    then
       if [ ! "$FORCE_INSTALL" = "y" ]
        then
            echo "Git is not found !"
            exit;
        fi
      echo '### install git'
      apt install git -y
    fi
}


function installApi() {
  cd "$INSTALL_DIR" || exit
  echo '### clone plaanet-api'
  git clone https://gitlab.com/plaanet2/plaanet-api.git
  cd plaanet-api || exit
  cp ./config/default.json ./config/development.json
  access_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
  cron_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
  sed -i "s/+access_pk+/$access_pk/" ./config/development.json
  sed -i "s/+cron_pk+/$cron_pk/" ./config/development.json
  mkdir ./public/uploads/avatar
  #sudo chown "$USER" ./public/uploads/avatar
  #sudo chown "$USER" ./public/uploads/post
  echo '### npm install plaanet-api'
  npm install
}

function installClient() {
  echo '### clone plaanet-client'
  cd "$INSTALL_DIR" || exit
  git clone https://gitlab.com/plaanet2/plaanet-client.git
  cd plaanet-client || exit
  echo '### npm install plaanet-client'
  cp ./src/config/default_tmp.json ./src/config/development.json
  cp ./.env.tmp ./.env
  npm install
}

function apiHelperMessage() {
  echo ''
  echo '> TO START API, USE FOLLOWING COMMANDES :'
  echo "cd $INSTALL_DIR/plaanet-api"
  echo 'node main.js'
  echo '>> then API is running on http://localhost:3000'
  echo ''
}

function clientHelperMessage() {
    echo ''
    echo '> TO START CLIENT, USE FOLLOWING COMMANDE :'
    echo "cd $INSTALL_DIR/plaanet-client"
    echo 'npm run serve'
    echo '>> then CLIENT is running on http://localhost:8080'
    echo ''
}

function askDirectory() {
  echo -n "Enter a directory: (${PWD})"
  read -r INSTALL_DIR

  if [ -z "$INSTALL_DIR" ]
  then
     INSTALL_DIR="$PWD"
  fi

  echo "install PLAANET into directory: $INSTALL_DIR"

  if [ ! -d "$INSTALL_DIR" ]
  then
    mkdir "$INSTALL_DIR"
  fi
}