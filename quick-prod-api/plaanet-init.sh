echo '***********************************'
echo 'LETS TRY TO BOOT THIS PLAANET-API !'
echo '***********************************'

path=`pwd`

echo '>>> Path to install : ' $path 
echo '>>> User : ' $USER

echo ''
echo ''

echo '######### FIRST : CLEAN OLD PROCESSES AND INSTALL FILES #########'
echo '>>> Stop plaanet-api service'
sudo systemctl stop plaanet-api.service
echo '>>> Remove plaanet-api service file'
sudo rm /etc/systemd/system/plaanet-api.service 
echo '>>> Stop Caddy - ReverseProxy'
sudo caddy stop
echo '######### CLEAN OK #########'
echo ''
echo ''



echo ""
echo "----------------------------"
echo "Configuration des paramètres"
echo "----------------------------"
echo ""
echo "Merci de fournir les paramètres que vous souhaitez appliquer à la configuration de votre API"
echo "Ces paramètres seront automatiquement appliqués, et votre serveur sera prêt à démarrer dans quelques secondes (si tout se passe bien ;)"

echo ""
echo "1- Nom de domaine, ou IP de votre instance sur le réseau - default : 192.168.1.33"
# le SSL / HTTPS est géré par Nginx, et est configuré automatiquement dans ce script
#my-api-to-test.io
echo "ex: my-api.io, ou 81.1.2.3 (httpS obligatoire)"
# récupère la valeur de chaque paramètre de configuration
echo ""
read -p "> Nom de domaine (sans https://) : " instance_domaine
instance_domaine=${instance_domaine:-unknown} 
echo "[$instance_domaine]"

if [ "$instance_domaine" = "unknown" ]
then
  echo "No domaine provided : exit"
  exit
fi

echo ""
echo "2- Port TCP associé à cette URL - default: 444"
read -p "> instance_port : " instance_port 
instance_port=${instance_port:-444} 
echo "[$instance_port]"


echo ""
echo "3- Port socket associé à cette URL - default: 445"
read -p "> instance_port_socket : " instance_port_socket 
instance_port_socket=${instance_port_socket:-445} 
echo "[$instance_port_socket]"


echo ""
echo "2- Port localhost - default: 3001"
read -p "> bind_port_https : " bind_port_https
bind_port_https=${bind_port_https:-3001} 
echo "[$bind_port_https]"

echo ""
echo "3- Port localhost socket - default: 3002"
read -p "> bind_port_socket : " bind_port_socket 
bind_port_socket=${bind_port_socket:-3002} 
echo "[$bind_port_socket]"


echo ""
echo "4- URL du noeud de référence - default: plaanet.io"
read -p "> root_node_url : (sans https://)" root_node_url 
root_node_url=${root_node_domain:-plaanet.io} 
echo "[$root_node_url]"

echo ""
echo "5- Port https du noeud de référence - default: 444"
read -p "> root_node_port : " root_node_port 
root_node_port=${root_node_port:-444} 
echo "[$root_node_port]"


echo ""
echo "6- Nom de la base de donnée (MongoDB) - default: plaanet"
read -p "> db_name : " db_name 
db_name=${db_name:-plaanet} 
echo "[$db_name]"

#TODO : configurer la Multichain dans le script de boot

echo ""
echo "7- Nom de la multichain (votes & sondage) - default: plaanet_prod"
read -p "> chain_name : " chain_name 
chain_name=${chain_name:-plaanet_prod} 
echo "[$chain_name]"

echo ""
echo "########################################################################################"
#echo "Merci ! Nous avons toutes les informations pour initialiser le système :)"
echo "Merci de patienter quelques instants, et de prier pour que le dieu des bugs nous protège"
echo "########################################################################################"
echo ""

####################################################################
####################################################################
####################################################################
####################################################################
####################################################################
####################################################################

#echo '### apt-get update'
#sudo apt-get update

echo ''
echo '### install mongodb'

wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

if [ -f /data ]
  echo '> /data directory already installed'
then
  sudo mkdir /data
  sudo mkdir /data/db
  sudo chown -R $USER:$USER /data
fi

echo ''
echo '### install multichain'

if [ -f /usr/local/bin/multichaind ]
then
  echo '> multichain already installed'
else 
  echo '> multichain not installed'

  sudo wget https://www.multichain.com/download/multichain-2.1.2.tar.gz
  tar -xvzf multichain-2.1.2.tar.gz
  cd multichain-2.1.2
  sudo mv multichaind multichain-cli multichain-util /usr/local/bin
fi

#si le répertoire d'installation passé en param1 existe
if [ -d $path ]
then
  #on se place dedant
  cd $path 
else
  #sinon on le créé, puis on se place dedant
  echo ">  le répertoire $path n'existe pas"
  mkdir $path
  echo "> le répertoire $path a été créé"
  cd $path
fi

if [ -f config-api-default.json ]
then
  rm config-api-default.json
  rm mongo.service
  rm multichain.service
fi


echo ''
echo '### download plaanet config files'
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/config-api-default.json
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/mongo.service
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/multichain.service

sudo cp $path/config-api-default.json $path/production.json

echo ''
echo '### clone plaanet-api'
if [ -d $path/plaanet-api ]
then
  echo "> le répertoire $path existe déjà"
else
  echo "> clonage dans $path"
  git clone https://gitlab.com/plaanet/plaanet-api.git
fi

cd $path/plaanet-api

#echo '### copy production config file'
#sudo cp $path/config-api-default.json ./config/production.json

if [ -d $path/plaanet-api/public/uploads/avatar ]
then
  echo "> le répertoire $path/plaanet-api/public/uploads/avatar existe déjà"
else
  sudo mkdir ./public/uploads/avatar
  echo "> répertoire $path/plaanet-api/public/uploads/avatar créé"
fi

sudo chown $USER ./public/uploads/avatar
sudo chown $USER ./public/uploads/post

echo ''
echo '### Create log directory'
if [ -d $path/logs ]
then
  echo 'directory $path/logs exists'
else
  mkdir $path/logs
  mkdir $path/logs/mongo
fi


echo ''
echo '### install build-essential ? (if needed)'
sudo apt-get install build-essential

echo ''
echo '### npm install plaanet-api'
sudo npm install

echo ''
echo '### create plaanet-api & mongo services for production'

rm $path/plaanet-api.service

# crée le fichier de service avec le custom path
echo "[Unit]
Description=plaanet API
[Service]
PIDFile=/tmp/plaanet-api-99.pid
User=root
Group=root
Restart=always
KillSignal=SIGQUIT
WorkingDirectory=$path/plaanet-api
ExecStart=sudo NODE_ENV=production /usr/bin/node $path/plaanet-api/main.js
StandardOutput=append:$path/logs/plaanet-api.log
StandardError=append:$path/logs/plaanet-api_error.log
[Install]
WantedBy=multi-user.target" >> $path/plaanet-api.service

#copie les services dans le repertoire des services 
sudo cp $path/plaanet-api.service /etc/systemd/system/plaanet-api.service
sudo cp $path/mongo.service /etc/systemd/system/mongo.service
#et définie les droits des fichiers
sudo chmod 755 /etc/systemd/system/plaanet-api.service
sudo chmod 755 /etc/systemd/system/mongo.service

echo '### enable services for production'
sudo systemctl enable plaanet-api
sudo systemctl enable mongo

echo '### start mongoDB'
sudo systemctl daemon-reload
sudo systemctl start mongo


echo ""
echo "### auto-generate random access key"
access_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
cron_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
echo "access_pk: " $access_pk
echo "cron_pk: " $cron_pk


#initialisation de la multichain
echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DE LA MULTICHAIN"
echo "------------------------------"
echo "> Voulez-vous initialiser la multichain ? "
echo ""
echo "Tapez 'n' pour ignorer l'initialisation"
read -p "> Installer la multichain ? (y/n) : " installChain 


rpcuser=""
rpcpassword=""

if [ "$installChain" != "n" ]
then
  echo ">>> Create multichain" $chain_name
  sudo multichain-util create $chain_name

  echo ">>> Stop old multichain process"
  sudo multichain-cli $chain_name stop
  sleep 2

  echo ">>> Start multichain process deamon"
  sudo multichaind $chain_name -daemon &

  echo ">>> wait 3 second please..."
  sleep 3

  echo ">>> Start creating multichain stream"
  sudo multichain-cli $chain_name create stream survey false
  sudo multichain-cli $chain_name create stream support false
  sudo multichain-cli $chain_name create stream vote false
  sudo multichain-cli $chain_name subscribe survey
  sudo multichain-cli $chain_name subscribe support
  sudo multichain-cli $chain_name subscribe vote



  #récupérer le user et mot de passe de la nouvelle blockchain créé:
  chainConfPath=""
  #si le fichier ~/.multichain/plaanet/multichain.conf exist
  if [ -f ~/.multichain/$chain_name/multichain.conf ]
  then
    chainConfPath=~/.multichain/$chain_name/multichain.conf
  elif [ -f /root/.multichain/$chain_name/multichain.conf ]
  then
    chainConfPath=/root/.multichain/$chain_name/multichain.conf
  else
    echo "error : no multichain configuration found on your system."
    echo "~/.multichain/$chain_name/multichain.conf - not exists"
    echo "/root/.multichain/$chain_name/multichain.conf - not exists"
    
    echo ""
    echo "Can you please provide the path to your multichain.conf file ?"
    read -p "> chainConfPath : " chainConfPath
    
    if [ -f "$chainConfPath/multichain.conf" ]
    then
      echo "Ok, this file exists ! Let's go !"
    else
      echo ""
      echo "Sorry, this path is not correct. Try again later bro !"
      echo "Aborting launching."
      echo "Auto-destruction in 5, 4, 3, 1 no 2, 1. Biiiiiiiiiiiiiiiiiiip !"
      exit
    fi
  fi

  #récupère le rpcuser et rpcpassword de la multichain précédemment configurée
  rpcuser=$(sudo head -n 1 $chainConfPath)
  rpcpassword=$(sudo tail -n 1 $chainConfPath)

  prefixu="rpcuser="
  rpcuser=${rpcuser#"$prefixu"}

  prefixp="rpcpassword="
  rpcpassword=${rpcpassword#"$prefixp"}

  echo "rpcuser: " $rpcuser
  echo "rpcpassword: " $rpcpassword

else 
  echo ">>> Multichain initialization ignored"
fi
# fin de l'initialisation de la multichain



#initialisation du fichier de config de l'api
echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DE L'API"
echo "------------------------------"


#instance_domaine = echo $instance_domaine | sed 's/\//\\\//g'
#root_node_url = echo $root_node_url | sed 's/\//\\\//g'

sed -i "s/+instance_domaine+/$instance_domaine/" $path/production.json
sed -i "s/+access_pk+/$access_pk/" $path/production.json
sed -i "s/+cron_pk+/$cron_pk/" $path/production.json
sed -i "s/+bind_port_https+/$bind_port_https/" $path/production.json
sed -i "s/+bind_port_socket+/$bind_port_socket/" $path/production.json
sed -i "s/+root_node_url+/$root_node_url/" $path/production.json
sed -i "s/+root_node_port+/$root_node_port/" $path/production.json
sed -i "s/+db_name+/$db_name/" $path/production.json
sed -i "s/+rpcuser+/$rpcuser/" $path/production.json
sed -i "s/+rpcpassword+/$rpcpassword/" $path/production.json
cp $path/production.json $path/plaanet-api/config/production.json

echo "------------------------------"


echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DU ReverseProxy - by Caddy Server"
echo "------------------------------"
echo "> Voulez-vous configurer le server Caddy en tant que ReverseProxy ? "
echo "(sécurité renforcée & mise en place automatique du protocol https)"
echo ""
echo "Tapez 'n' pour ignorer la configuration"
read -p "> Installer le server Caddy ? (y/n) : " installCaddy 

if [ "$installCaddy" != "n" ]
then

  #install caddy server
  echo ''
  echo '### install caddy server'
  sudo apt install curl
  sudo apt install libnss3-tools
  sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
  curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
  curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
  sudo apt update
  sudo apt install caddy

  echo ">>> create config file $path/Caddyfile"
  if [ -f $path/Caddyfile ]
  then
    sudo rm $path/Caddyfile
  fi

  touch $path/Caddyfile
  sudo chmod 777 $path/Caddyfile
  echo "$instance_domaine:$instance_port {\nreverse_proxy 127.0.0.1:$bind_port_https\n}\n\n$instance_domaine:$instance_port_socket {\nreverse_proxy 127.0.0.1:$bind_port_socket\n}\n\n" >> $path/Caddyfile

  echo ">>> Config Caddy Ok"
  sudo chmod 644 $path/Caddyfile

  echo ""
  echo "----------------------------------"
  echo "LANCEMENT DU REVERSE_PROXY - CADDY"
  echo "----------------------------------"
  cd $path
  sudo caddy reload
  sudo caddy start

else 
  echo ">>> Caddy installation ignored"
fi

echo ""
echo "----------------------------------"
echo "LANCEMENT DE L'API PLAANET"
echo "----------------------------------"
sudo systemctl daemon-reload
sudo systemctl start plaanet-api

echo ""
echo ""
echo "----------------------------------"
echo "ALO HUSTON"
echo "----------------------------------"
echo "Si tout s'est bien déroulé, vous devriez être en mesure d'obtenir une réponse à l'adresse suivante :"
echo "https://$instance_domaine:$instance_port/instance/exists"
echo "----------------------------------"
echo "En cas d'échec de la mission, tentez de terminer l'amarage de la station manuellement"
echo "Et de revenir sur Terre sain et sauf."
echo "-_-_-_--_-_-_--_-_-_-"
echo "À vous Roger. Terminé"
echo "-_-_-_--_-_-_--_-_-_-"