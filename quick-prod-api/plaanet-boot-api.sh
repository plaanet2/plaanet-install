echo '***********************************'
echo 'LETS TRY TO BOOT THIS PLAANET-API !'
echo '***********************************'

echo '>>> Path to install : ' $1
echo '>>> User : ' $2
echo ''
echo ''

echo '######### FIRST : CLEAN OLD PROCESSES AND INSTALL FILES #########'
echo '>>> Stop plaanet-api service'
sudo systemctl stop plaanet-api.service
echo '>>> Remove plaanet-api service file'
sudo rm /etc/systemd/system/plaanet-api.service 
echo '>>> Stop Caddy - ReverseProxy'
sudo caddy stop
echo '######### CLEAN OK #########'
echo ''
echo ''


#echo '### apt-get update'
#sudo apt-get update

echo '### install mongodb'

wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get install -y mongodb-org

if [ -f /data ]
  echo '> /data directory already installed'
then
  sudo mkdir /data
  sudo mkdir /data/db
  sudo chown -R $2:$2 /data
fi

echo '### install multichain'

if [ -f /usr/local/bin/multichaind ]
then
  echo '> multichain already installed'
else 
  echo '> multichain not installed'

  sudo wget https://www.multichain.com/download/multichain-2.1.2.tar.gz
  tar -xvzf multichain-2.1.2.tar.gz
  cd multichain-2.1.2
  sudo mv multichaind multichain-cli multichain-util /usr/local/bin
fi

#si le répertoire d'installation passé en param1 existe
if [ -d $1 ]
then
  #on se place dedant
  cd $1 
else
  #sinon on le créé, puis on se place dedant
  echo ">  le répertoire $1 n'existe pas"
  mkdir $1
  echo "> le répertoire $1 a été créé"
  cd $1
fi

if [ -f config-api-default.json ]
then
  rm config-api-default.json
  rm mongo.service
  rm multichain.service
fi

# echo '### install nginx'
# if [ -d /etc/nginx/sites-enabled ]
# then
#   echo '> nginx already installed'
# else
#   sudo apt-get install nginx
# fi


echo '### download plaanet config files'
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/config-api-default.json
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/mongo.service
wget -q https://gitlab.com/plaanet/plaanet-install/-/raw/master/quick-prod-api/multichain.service


echo '### clone plaanet-api'
if [ -d $1/plaanet-api ]
then
  echo "> le répertoire $1 existe déjà"
else
  echo "> clonage dans $1"
  git clone https://gitlab.com/plaanet/plaanet-api.git
fi

cd $1/plaanet-api

#echo '### copy production config file'
#sudo cp $1/config-api-default.json ./config/production.json

if [ -d $1/plaanet-api/public/uploads/avatar ]
then
  echo "> le répertoire $1/plaanet-api/public/uploads/avatar existe déjà"
else
  sudo mkdir ./public/uploads/avatar
  echo "> répertoire $1/plaanet-api/public/uploads/avatar créé"
fi

sudo chown $2 ./public/uploads/avatar
sudo chown $2 ./public/uploads/post

echo '### Create log directory'
if [ -d $1/logs ]
then
  echo 'directory $1/logs exists'
else
  mkdir $1/logs
  mkdir $1/logs/mongo
fi

echo '### npm install plaanet-api'
sudo npm install

echo '### create plaanet-api & mongo services for production'

rm $1/plaanet-api.service

# crée le fichier de service avec le custom path
echo "[Unit]
Description=plaanet API
[Service]
PIDFile=/tmp/plaanet-api-99.pid
User=root
Group=root
Restart=always
KillSignal=SIGQUIT
WorkingDirectory=$1/plaanet-api
ExecStart=sudo NODE_ENV=production /usr/bin/node $1/plaanet-api/main.js
StandardOutput=append:$1/logs/plaanet-api.log
StandardError=append:$1/logs/plaanet-api_error.log
[Install]
WantedBy=multi-user.target" >> $1/plaanet-api.service

#copie les services dans le repertoire des services 
sudo cp $1/plaanet-api.service /etc/systemd/system/plaanet-api.service
sudo cp $1/mongo.service /etc/systemd/system/mongo.service
#et définie les droits des fichiers
sudo chmod 755 /etc/systemd/system/plaanet-api.service
sudo chmod 755 /etc/systemd/system/mongo.service

echo '### enable services for production'
sudo systemctl enable plaanet-api
sudo systemctl enable mongo

echo '### start mongoDB'
sudo systemctl daemon-reload
sudo systemctl start mongo

echo ''
echo ''
echo '***********'
echo 'BOOT DONE !'
echo '***********'
echo '> YOU ARE READY TO CONFIGURE PARAMETERS !'
echo '> sudo ./plaanet-launch-api.sh'