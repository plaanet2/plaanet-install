pathXXX=`pwd`
echo "$pathXXX"

exit

echo ""
echo "1- Nom de domaine, ou IP de votre instance sur le réseau - default : 192.168.1.33"
# le SSL / HTTPS est géré par Nginx, et est configuré automatiquement dans ce script
#my-api-to-test.io
echo "ex: my-api.io, ou 81.1.2.3 (httpS obligatoire)"
# récupère la valeur de chaque paramètre de configuration
echo ""
read -p "> instance_domaine : " instance_domaine
instance_domaine=${instance_domaine:-my-api.io} 
echo "[$instance_domaine]"

echo ""
echo "2- Port TCP associé à cette URL - default: 444"
read -p "> instance_port : " instance_port 
instance_port=${instance_port:-444} 
echo "[$instance_port]"


echo ""
echo "3- Port socket associé à cette URL - default: 445"
read -p "> instance_port_socket : " instance_port_socket 
instance_port_socket=${instance_port_socket:-445} 
echo "[$instance_port_socket]"


echo ""
echo "2- Port du serveur local - default: 3001"
read -p "> bind_port_https : " bind_port_https
bind_port_https=${bind_port_https:-3001} 
echo "[$bind_port_https]"

echo ""
echo "3- Port socket - default: 3002"
read -p "> bind_port_socket : " bind_port_socket 
bind_port_socket=${bind_port_socket:-3002} 
echo "[$bind_port_socket]"


echo ">>> create config file $1/Caddyfile"
if [ -f Caddyfile ]
then
  sudo rm Caddyfile
fi
  
touch Caddyfile
sudo chmod 777 Caddyfile
echo "$instance_domaine:$instance_port {\nreverse_proxy 127.0.0.1:$bind_port_https\n}\n\n$instance_domaine:$instance_port_socket {\nreverse_proxy 127.0.0.1:$bind_port_socket\n}\n\n" >> Caddyfile

echo ">>> Config Caddy Ok"