echo "***********************************"
echo "LETS TRY TO LAUNCH THIS PLAANET-API !"
echo "***********************************"

echo ''
echo ''
echo '######### FIRST : CLEAN OLD PROCESSES AND INSTALL FILES #########'
echo '>>> Stop Caddy - ReverseProxy'
sudo caddy stop
echo '>>> Stop Plaanet Api'
sudo systemctl stop plaanet-api
echo '######### CLEAN OK #########'
echo ''
echo ''

path_api=$1
echo '>>> Path to install : ' $path_api
path_api=$(echo $path_api|sed 's/\//\\\//g')
echo '>>> Path to install escaped : ' $path_api

echo ""
echo ">>> auto-generate random access key"
access_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
cron_pk=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w52 | head -n1)
echo "access_pk: " $access_pk
echo "cron_pk: " $cron_pk



echo ""
echo "----------------------------"
echo "Configuration des paramètres"
echo "----------------------------"
echo ""
echo "Merci de fournir les paramètres que vous souhaitez appliquer à la configuration de votre API"
echo "Ces paramètres seront automatiquement appliqués, et votre serveur sera prêt à démarrer dans quelques secondes (si tout se passe bien ;)"



echo ""
echo "1- Nom de domaine, ou IP de votre instance sur le réseau - default : 192.168.1.33"
# le SSL / HTTPS est géré par Nginx, et est configuré automatiquement dans ce script
#my-api-to-test.io
echo "ex: my-api.io, ou 81.1.2.3 (httpS obligatoire)"
# récupère la valeur de chaque paramètre de configuration
echo ""
read -p "> instance_domaine : " instance_domaine
instance_domaine=${instance_domaine:-192.168.1.33} 
echo "[$instance_domaine]"

echo ""
echo "2- Port TCP associé à cette URL - default: 444"
read -p "> instance_port : " instance_port 
instance_port=${instance_port:-444} 
echo "[$instance_port]"


echo ""
echo "3- Port socket associé à cette URL - default: 445"
read -p "> instance_port_socket : " instance_port_socket 
instance_port_socket=${instance_port_socket:-445} 
echo "[$instance_port_socket]"


echo ""
echo "2- Port du serveur local - default: 3001"
read -p "> bind_port_https : " bind_port_https
bind_port_https=${bind_port_https:-3001} 
echo "[$bind_port_https]"

echo ""
echo "3- Port socket - default: 3002"
read -p "> bind_port_socket : " bind_port_socket 
bind_port_socket=${bind_port_socket:-3002} 
echo "[$bind_port_socket]"


echo ""
echo "4- URL du noeud de référence - default: https://plaanet.io"
read -p "> root_node_url : " root_node_url 
root_node_url=${root_node_url:-https://plaanet.io} 
echo "[$root_node_url]"

echo ""
echo "5- Port https du noeud de référence - default: 444"
read -p "> root_node_port : " root_node_port 
root_node_port=${root_node_port:-444} 
echo "[$root_node_port]"


echo ""
echo "6- Nom de la base de donnée (MongoDB) - default: plaanet"
read -p "> db_name : " db_name 
db_name=${db_name:-plaanet} 
echo "[$db_name]"

#TODO : configurer la Multichain dans le script de boot

echo ""
echo "7- Nom de la multichain (votes & sondage) - default: plaanet_prod"
read -p "> chain_name : " chain_name 
chain_name=${chain_name:-plaanet_prod} 
echo "[$chain_name]"

echo ""
#echo "Merci ! Nous avons toutes les informations pour initialiser le système :)"
echo "Merci de patienter quelques instants, et de prier pour que le dieu des bugs nous protège"




#initialisation de la multichain
echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DE LA MULTICHAIN"
echo "------------------------------"
echo "> Voulez-vous initialiser la multichain ? "
echo ""
echo "Tapez 'n' pour ignorer l'initialisation"
read -p "> Installer la multichain ? (y/n) : " installChain 


rpcuser=""
rpcpassword=""

if [ "$installChain" != "n" ]
then
  echo ">>> Create multichain" $chain_name
  sudo multichain-util create $chain_name

  echo ">>> Stop old multichain process"
  sudo multichain-cli $chain_name stop
  sleep 2

  echo ">>> Start multichain process deamon"
  sudo multichaind $chain_name -daemon &

  echo ">>> wait 3 second please..."
  sleep 3

  echo ">>> Start creating multichain stream"
  sudo multichain-cli $chain_name create stream survey false
  sudo multichain-cli $chain_name create stream support false
  sudo multichain-cli $chain_name create stream vote false
  sudo multichain-cli $chain_name subscribe survey
  sudo multichain-cli $chain_name subscribe support
  sudo multichain-cli $chain_name subscribe vote



  #récupérer le user et mot de passe de la nouvelle blockchain créé:
  chainConfPath=""
  #si le fichier ~/.multichain/plaanet/multichain.conf exist
  if [ -f ~/.multichain/$chain_name/multichain.conf ]
  then
    chainConfPath=~/.multichain/$chain_name/multichain.conf
  elif [ -f /root/.multichain/$chain_name/multichain.conf ]
  then
    chainConfPath=/root/.multichain/$chain_name/multichain.conf
  else
    echo "error : no multichain configuration found on your system."
    echo "~/.multichain/$chain_name/multichain.conf - not exists"
    echo "/root/.multichain/$chain_name/multichain.conf - not exists"
    
    echo ""
    echo "Can you please provide the path to your multichain.conf file ?"
    read -p "> chainConfPath : " chainConfPath
    
    if [ -f "$chainConfPath/multichain.conf" ]
    then
      echo "Ok, this file exists ! Let's go !"
    else
      echo ""
      echo "Sorry, this path is not correct. Try again later bro !"
      echo "Aborting launching."
      echo "Auto-destruction in 5, 4, 3, 1 no 2, 1. Biiiiiiiiiiiiiiiiiiip !"
      exit
    fi
  fi

  #récupère le rpcuser et rpcpassword de la multichain précédemment configurée
  rpcuser=$(sudo head -n 1 $chainConfPath)
  rpcpassword=$(sudo tail -n 1 $chainConfPath)

  prefixu="rpcuser="
  rpcuser=${rpcuser#"$prefixu"}

  prefixp="rpcpassword="
  rpcpassword=${rpcpassword#"$prefixp"}

  echo "rpcuser: " $rpcuser
  echo "rpcpassword: " $rpcpassword

else 
  echo ">>> Multichain initialization ignored"
fi
# fin de l'initialisation de la multichain



#initialisation du fichier de config de l'api
echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DE L'API"
echo "------------------------------"


instance_domaine = echo $instance_domaine | sed 's/\//\\\//g'
root_node_url = echo $root_node_url | sed 's/\//\\\//g'

cp config-api-default.json production.json
sed -i "s/+instance_domaine+/$instance_domaine/" production.json
sed -i "s/+access_pk+/$access_pk/" production.json
sed -i "s/+cron_pk+/$cron_pk/" production.json
sed -i "s/+bind_port_https+/$bind_port_https/" production.json
sed -i "s/+bind_port_socket+/$bind_port_socket/" production.json
sed -i "s/+root_node_url+/$root_node_url/" production.json
sed -i "s/+root_node_port+/$root_node_port/" production.json
sed -i "s/+db_name+/$db_name/" production.json
sed -i "s/+rpcuser+/$rpcuser/" production.json
sed -i "s/+rpcpassword+/$rpcpassword/" production.json
cp production.json $1/plaanet-api/config/production.json
echo "> ok"
echo "------------------------------"


echo ""
echo ""
echo "------------------------------"
echo "CONFIGURATION DU ReverseProxy - by Caddy Server"
echo "------------------------------"
echo "> Voulez-vous configurer le server Caddy en tant que ReverseProxy ? "
echo "(sécurité renforcée & mise en place automatique du protocol https)"
echo ""
echo "Tapez 'n' pour ignorer la configuration"
read -p "> Installer le server Caddy ? (y/n) : " installCaddy 

if [ "$installCaddy" != "n" ]
then

  #install caddy server
  echo '### install caddy server'
  sudo apt install curl
  sudo apt install libnss3-tools
  sudo apt install -y debian-keyring debian-archive-keyring apt-transport-https
  curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
  curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
  sudo apt update
  sudo apt install caddy

  echo ">>> create config file $1/Caddyfile"
  if [ -f $1/Caddyfile ]
  then
    sudo rm $1/Caddyfile
  fi

  touch $1/Caddyfile
  sudo chmod 777 $1/Caddyfile
  echo "$instance_domaine:$instance_port {\nreverse_proxy 127.0.0.1:$bind_port_https\n}\n\n$instance_domaine:$instance_port_socket {\nreverse_proxy 127.0.0.1:$bind_port_socket\n}\n\n" >> $1/Caddyfile

  echo ">>> Config Caddy Ok"
  sudo chmod 644 $1/Caddyfile

  echo ""
  echo "----------------------------------"
  echo "LANCEMENT DU REVERSE_PROXY - CADDY"
  echo "----------------------------------"
  cd $1
  sudo caddy reload
  sudo caddy start

else 
  echo ">>> Caddy installation ignored"
fi

echo ""
echo "----------------------------------"
echo "LANCEMENT DE L'API PLAANET"
echo "----------------------------------"
sudo systemctl daemon-reload
sudo systemctl start plaanet-api

echo ""
echo ""
echo "----------------------------------"
echo "ALO HUSTON"
echo "----------------------------------"
echo "Si tout s'est bien déroulé, vous devriez être en mesure d'obtenir une réponse à l'adresse suivante :"
echo "https://$instance_domaine/instance/exists"
echo "----------------------------------"
echo "En cas d'échec de la mission, tentez de terminer l'amarage de la station manuellement"
echo "Et de revenir sur Terre sain et sauf."
echo "-_-_-_--_-_-_--_-_-_-"
echo "À vous Roger. Terminé"
echo "-_-_-_--_-_-_--_-_-_-"