
## plaanet-install
### All you need to install your own Plaanet !
---
This package contains 2 folders : dev & prod

Use **/dev** for local test & developpement
Use **/prod** for production

- The production installation will automaticaly install all services files, needed to run the app with no interruption *(this process has not been totaly tested and approved yet)*
- The dev installation will not install this services files, and let you start api and client manualy, with commande lines.


### Let's install it !

Clone this repository in /home/ubuntu/ 

In all file, replace **ubuntu** by your own user name.

Then, all you have to do is to choose Dev or Prod, and then run ONE of this file :

- sudo ./plaanet-boot-all.sh > to install API & Client in one shot
- sudo ./plaanet-boot-api.sh > to install only API
- sudo ./plaanet-boot-client.sh > to install only Client

It will install all packages needed to run the app (included mongodb, git, etc)
(so, you can use it in an empty ubuntu server, after a new OS installation for exemple)


 ---
#### Using SSL/https : 
If you want to use https, copy your certificates in the folder plaanet-api/ssl 

And, in the api config file (plaanet-api/config/defaut.json) : complete parameters with the corresponding file name :

ssl_key, ssl_cert, ssl_ca

Then, restart the api service : sudo systemctl restart plaanet-client.service
 
 ---
#### Start app in DEV : 
To run the app using the Dev process (without auto services) run this commands to start the app :

- **API** : 
    cd /home/ubuntu/Node/plaanet-api

    node main.js

##### Then API is running on http://localhost:3000
    
- **Client** 
    cd /home/ubuntu/Node/plaanet-client

    npm run serve -- --port 8081
        
##### Then Client is running on http://localhost:8081

---
#### Boot your Node !
When all the app is runing, you have to init the node in database.

Go to : http://localhost:8081/boot-node 

Enter all your node parameters.
And Click on "Boot" !

#### Init Geospacial indexes in MongoDB
In a terminal, enter in mongodb by taping the commande : `mongo`

Then, enter this commande : (needed to execute mongo geospacial queries)

    db.instances.createIndex( { position : "2dsphere" } )
    exit


    db.publications.createIndex( { position : "2dsphere" } )
    exit

---
#### Create your first account
Now, you can go back to https://localhost:8081 , and create your first account !

---

> You can modify all this files to create your own installation, according to your system, and the way you want to manage the application.
---
